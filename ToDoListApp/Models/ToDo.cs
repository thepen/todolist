﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToDoListApp.Models
{
    public class ToDo
    {
        public int id { get; set; }

        public string title { get; set; }
        public string description { get; set; }

        public DateTime? reminder { get; set; }
        public bool isDone { get; set; }
        public virtual ApplicationUser user { get; set; }
    }
}