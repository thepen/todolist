﻿$(document).ready(function () {
    $('.checkboxes').change(function () {
        var self = $(this);
        var id = self.attr('id')
        var value = self.prop('checked')

        $.ajax({
            url: '/ToDoes/AjaxEdit',
            data: {
                id:id,
                value:value
            },
            type: 'POST',
            success: function (result) {
                $('#dynamictable').html(result)
            }
        })

    })
})