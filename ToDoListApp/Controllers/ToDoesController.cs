﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Helpers;
using System.Web.Mvc;
using ToDoListApp.Models;

namespace ToDoListApp.Controllers
{
    [System.Runtime.InteropServices.Guid("BDF27A89-253C-41C6-9D87-A8178B0B6191")]
    public class ToDoesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ToDoes
        public ActionResult Index()
        {
            SendEmail();

            return View();

        }
        private IEnumerable<ToDo> GetListOfToDo()
        {
            string currentUserId = User.Identity.GetUserId();
            ApplicationUser currentUser = db.Users.FirstOrDefault(x => x.Id == currentUserId);
            return db.toDos.ToList().Where(x => x.user == currentUser).OrderByDescending(x => x.id);
        }
        public ActionResult DyanmicTable()
        {
            return PartialView("_TodoAjaxTable", GetListOfToDo());
        }
        // GET: ToDoes/Details/5
        public ActionResult Details(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ToDo toDo = db.toDos.Find(id);
            if(toDo == null)
            {
                return HttpNotFound();
            }
            return View(toDo);
        }

        // GET: ToDoes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ToDoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,title,description,reminder,isDone")] ToDo toDo)
        {
            if(ModelState.IsValid)
            {
                if(toDo.reminder == null)
                {

                    toDo.reminder = DateTime.Now;
                }

                string currentUserId = User.Identity.GetUserId();
                ApplicationUser currentUser = db.Users.FirstOrDefault(x => x.Id == currentUserId);
                toDo.user = currentUser;
                db.toDos.Add(toDo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(toDo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateAjaxForm([Bind(Include = "id,title,description,reminder")] ToDo toDo)
        {
            if(ModelState.IsValid)
            {
                if(toDo.reminder == null)
                {

                    toDo.reminder = DateTime.Now;
                }

                string currentUserId = User.Identity.GetUserId();
                ApplicationUser currentUser = db.Users.FirstOrDefault(x => x.Id == currentUserId);
                toDo.user = currentUser;
                toDo.isDone = false;
                db.toDos.Add(toDo);
                db.SaveChanges();
                //return RedirectToAction("Index");
            }

            return PartialView("_TodoAjaxTable", GetListOfToDo());
        }
        // GET: ToDoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ToDo toDo = db.toDos.Find(id);
            if(toDo == null)
            {
                return HttpNotFound();
            }

            string currentUserId = User.Identity.GetUserId();
            ApplicationUser currentUser = db.Users.FirstOrDefault(x => x.Id == currentUserId);
            toDo.user = currentUser;
            if(toDo.user != currentUser)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            return View(toDo);
        }

        // POST: ToDoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,title,description,reminder,isDone")] ToDo toDo)
        {
            if(ModelState.IsValid)
            {
                db.Entry(toDo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(toDo);
        }

        [HttpPost]

        public ActionResult AjaxEdit(int? id, bool value)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ToDo toDo = db.toDos.Find(id);
            if(toDo == null)
            {
                return HttpNotFound();
            }
            else
            {
                toDo.isDone = value;
                db.Entry(toDo).State = EntityState.Modified;
                db.SaveChanges();
            }

            return PartialView("_TodoAjaxTable", GetListOfToDo());
        }


        // GET: ToDoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ToDo toDo = db.toDos.Find(id);
            if(toDo == null)
            {
                return HttpNotFound();
            }
            return View(toDo);
        }

        // POST: ToDoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ToDo toDo = db.toDos.Find(id);
            db.toDos.Remove(toDo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if(disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        //this should be in a scheduller
        public ActionResult SendEmail()
        {

            return View();
        }

        [HttpPost]
        public ActionResult SendEmail(ToDo obj)
        {
            string currentUserId = User.Identity.GetUserId();
            ApplicationUser currentUser = db.Users.FirstOrDefault(x => x.Id == currentUserId);
            var reminder = db.toDos.ToList().Where(x => x.user == currentUser && x.isDone == false).OrderByDescending(x => x.id);
            foreach(var item in reminder)
            {
                TimeSpan hours = DateTime.Now.Subtract(Convert.ToDateTime(item.reminder));
                if(hours.Hours > 12)
                {
                    try
                    {
                        //Configuring webMail class to send emails  
                        //gmail smtp server  
                        WebMail.SmtpServer = "smtp.gmail.com";
                        //gmail port to send emails  
                        WebMail.SmtpPort = 587;
                        WebMail.SmtpUseDefaultCredentials = true;
                        //sending emails with secure protocol  
                        WebMail.EnableSsl = true;
                        //EmailId used to send emails from application  
                        WebMail.UserName = "thepen0411@gmail.com";
                        WebMail.Password = "";

                        //Sender email address.  
                        WebMail.From = "thepen0411@gmail.com";

                        //Send email  
                        WebMail.Send(to: currentUser.Email, subject: obj.title, body: obj.description, isBodyHtml: true);
                        ViewBag.Status = "Email Sent Successfully.";
                    }
                    catch(Exception ex)
                    {
                        ViewBag.Status = "Problem while sending email, Please check details.";
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                    }

                }
            }

            return View();

        }
    }
}

